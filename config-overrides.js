const {
  override,
  babelInclude,
  addBabelPlugin,
  addWebpackPlugin
} = require("customize-cra");
const webpack = require("webpack");

const path = require("path");

module.exports = override(
  addBabelPlugin("react-native-web"),
  babelInclude([
    path.resolve("src")
  ]),
  addWebpackPlugin(
    new webpack.DefinePlugin({
      __DEV__: process.env.NODE_ENV_ROOT === "development"
    })
  )
);